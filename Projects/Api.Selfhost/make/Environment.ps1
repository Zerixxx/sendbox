$BUILD_VERSION = "$($Context.Version).0"
$RELEASE_VERSION = $Context.Version

$NUGET_PACK_OUTPUT = 'NuGetPackOutput'

$TOOLS_PACKAGE = "Api.SelfHost.BuildTools"
$API_PACKAGE = "Api.SelfHost"

### Artifactory
$NUGET_SERVER_URL="http://localhost/NugetServ/"
#$ARTIFACTORY_API_KEY = 'build.user:xyz123'

### Octopus
$OCTO_TOOLS_VERSION = "3.3.18"
$OCTO_DEPLOY_RUNNER_VERSION = "1.0.0.8"
$OCTO_DEPLOY_TOOLS_VERSION = "2.2.13"
$OCTO_PROJECT = "Test Api"
$OCTOPUS_URL = "http://localhost:8090/"
$OCTOPUS_API_KEY = "API-6DZFD9UFP0PMHPCRSPSIMUELTE"
