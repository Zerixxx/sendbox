Define-Step -Name 'Update version info' -Target 'LOCAL,BUILD' -Body {
	. (require 'psmake.mod.update-version-info')
	Update-VersionInAssemblyInfo $BUILD_VERSION
}

Define-Step -Name 'Building' -Target 'LOCAL,BUILD' -Body {
	call "$($Context.NuGetExe)" restore Api.SelfHost.sln -ConfigFile "$($Context.NuGetConfig)"
	call "${env:ProgramFiles(x86)}\MSBuild\12.0\Bin\MSBuild.exe" Api.SelfHost.sln /t:"Clean,Build" /p:Configuration=Release /m /verbosity:m /nologo /p:TreatWarningsAsErrors=true
}

Define-Step -Name 'Testing' -Target 'LOCAL,BUILD' -Body {
	. (require 'psmake.mod.testing')
	
	$tests = @()
	$tests += Define-MsTests -GroupName 'Unit tests' -TestAssembly "*\bin\Release\*.SelfHost.Tests.dll"

	try {
		$tests | Run-Tests -EraseReportDirectory -Cover -CodeFilter '+[Api*]* -[*Tests*]*' -TestFilter '*Tests.dll' | Generate-CoverageSummary | Check-AcceptableCoverage -AcceptableCoverage 0
	}
	finally{
		if(Test-Path HealthMonitorAlerting.AcceptanceTests\bin\Release\Reports)
		{
			cp HealthMonitorAlerting.AcceptanceTests\bin\Release\Reports\FeaturesSummary.* reports 
		}
	}
}

Define-Step -Name 'Packaging' -Target 'LOCAL,BUILD' -Body {
	. (require 'psmake.mod.packaging')

	rm -recurse -force $NUGET_PACK_OUTPUT -ErrorAction SilentlyContinue | Out-Null
    mkdir $NUGET_PACK_OUTPUT | Out-Null
    	
    Package-DeployableNuSpec "$TOOLS_PACKAGE.nuspec" -Version $BUILD_VERSION -NoDefaultExcludes $true -Output $NUGET_PACK_OUTPUT
    Package-DeployableNuSpec "$API_PACKAGE.nuspec" -Version $BUILD_VERSION -NoDefaultExcludes $true -Output $NUGET_PACK_OUTPUT
}

Define-Step -Name "Publish packages" -Target "BUILD" -Body {
	function Push-Package($packageName, $packageVersion, $artifactoryUrl) {
		call $Context.NugetExe push "$packageName.$packageVersion.nupkg" -Source $artifactoryUrl -ApiKey "" -ConfigFile $Context.NuGetConfig
	}

	Write-Status "Publishing deploy packages to $NUGET_SERVER_URL"
    Push-Package "$NUGET_PACK_OUTPUT\$TOOLS_PACKAGE" $BUILD_VERSION $NUGET_SERVER_URL
	Push-Package "$NUGET_PACK_OUTPUT\$API_PACKAGE" $BUILD_VERSION $NUGET_SERVER_URL
}

Define-Step -Name "Deploy" -Target "Dev" -Body {
	$octoPath = Fetch-Package Wonga.OctopusDeployRunner $OCTO_DEPLOY_RUNNER_VERSION
	
	& "$octoPath\Wonga.OctopusDeployRunner.exe" `
		--project $OCTO_PROJECT `
		--releaseVersion $RELEASE_VERSION `
		--environment $OCTO_DEPLOYMENT `
		--server $env:OctopusServerUrl `
		--apiKey $env:UkpeOctopusApiKey
		
	if(!$?) { throw "Octopus deployment failed." }
}

Define-Step -Name "Create release" -Target "BUILD" -Body {
	$octoPath = Fetch-Package OctopusTools $OCTO_TOOLS_VERSION
	
	& "$octoPath\tools\Octo.exe" create-release `
		--project $OCTO_PROJECT `
		--version "$BUILD_VERSION" `
		--package "$($API_PACKAGE):$BUILD_VERSION" `
		--package "Wonga.DeployTools:$OCTO_DEPLOY_TOOLS_VERSION" `
		--server $OCTOPUS_URL `
		--apiKey $OCTOPUS_API_KEY
		
	if(!$?) { throw "Octopus release creation failed." }
}
